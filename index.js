process.on('uncaughtException', err => console.error(err.stack));
process.on('unhandledRejection', err => console.error(`Uncaught Promise Rejection:\n${err.stack}`));

module.exports = {
	Bot: require('./lib/Bot'),
	CommandHandler: require('./lib/CommandHandler'),
	ReactionHandler: require('./lib/ReactionHandler'),
	Utils: require('./lib/Utils'),
	RichEmbed: require('discord.js').RichEmbed,
	UI: require('./lib/ui')
};

process.on('SIGINT', () => module.exports.Bot.stopAll().then(() => process.exit()));