const PQueue = require('p-queue');

const Logger = require('./Logger');

module.exports = class ReactionHandler {
	constructor(name, emojis){
		this._name = name;
		this._emojis = emojis;
	    this._handlers = {};
	}
	listen(message, callbacks, user, remove, bots, logLevel, file){
		this._handlers[message.id] = {
			callbacks,
			user: user ? (user.id || user) : null,
			remove,
			bots,
			logLevel,
			logger: new Logger(this._name, logLevel, file)
		};
		return ReactionHandler.react(message, this._emojis);
	}
	_handle(reaction, user, me){
		const
			emoji = reaction.emoji.toString(),
			message = reaction.message.id,
			handler = this._handlers[message];
		if(
			user.id === me
			||
			this._emojis.indexOf(emoji) === -1
			||
			!handler
			||
			(handler.user && handler.user !== user.id)
			||
			user.bot && !handler.bots
		) return;
		handler.logger.log(3, `Reaction handled : '${emoji}' on ${message} by ${user.tag} (${user.id})`);
		const remove = () => { if(handler.remove) reaction.remove(user); };
		try {
			handler.callbacks.find(c => c.emoji === emoji).action(remove).then(remove);
		} catch(_){
		    remove();
        }
	}
	static react(message, reactions){
		return new Promise((resolve, reject) => {
			if(Array.isArray(reactions))
                new PQueue({ concurrency: 1 }).addAll(reactions.map(reaction => () => message.react(reaction))).then(resolve).catch(reject);
			else
				message.react(reactions).then(resolve).catch(reject);
		});
	}
};
