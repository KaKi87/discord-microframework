const
	console_stamp = require('console-stamp'),
	fs = require('fs');

const loggingOptions = {
	pattern: 'dd/mm/yy HH:MM:ss.l',
	colors: {
		stamp: 'yellow',
		label: 'green'
	}
};

console_stamp(console, loggingOptions);

module.exports = class Logger {
	constructor(name, level, file = `./${name}.log`){
		this._name = name;
		this._level = ['none', 'error', 'warn', 'info'].indexOf(level);
		if(file){
			this._logFile = new console.Console(fs.createWriteStream(file));
			console_stamp(this._logFile, loggingOptions);
		}
	}
	log(level, message){
		if(this._level < level) return;
		message = (this._name ? `(${this._name}) ` : '') + message;
		switch(level){
			case 1:
				console.error(message);
				break;
			case 2:
				console.warn(message);
				break;
			case 3:
				console.info(message);
				break;
		}
		if(this._logFile){
			switch(level){
				case 1:
					this._logFile.error(message);
					break;
				case 2:
					this._logFile.warn(message);
					break;
				case 3:
					this._logFile.info(message);
					break;
			}
		}
	}
};
