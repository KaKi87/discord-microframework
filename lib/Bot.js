const
	Discord = require('discord.js');

const Logger = require('./Logger');

class Bot {
	constructor(name, token, logLevel){
		if(!name)
			throw 'Bot name required';
		if(!token)
			throw 'Bot token required';
		if(Bot.instances.find(bot => bot._name === name))
			throw 'Bot name already exists';
		if(Bot.instances.find(bot => bot._token === token))
			throw 'Bot token already exists';
		this._name = name;
		this._token = token;
		this._log = new Logger(name, logLevel || 'error').log;
		this._client = new Discord.Client();
		this._client.on('error', err => this._log(1, err));
		this._commandHandlers = [];
		this._client.on('message', message => this._commandHandlers.forEach(handler => handler._handle(message, this._client.user.id)));
		this._reactionHandlers = [];
		this._client.on('messageReactionAdd', (reaction, user) => this._reactionHandlers.forEach(handler => handler._handle(reaction, user, this._client.user.id)));
		Bot.instances.push(this);
		this._log(3, `Bot instanciated (total: ${Bot.instances.length})`);
	}
	start(){
		return new Promise((resolve, reject) => {
			this._client.on('ready', () => {
				this._log(3, 'Bot started');
				resolve();
			});
			this._client.login(this._token).catch(err => {
				this._log(1, `Bot failed to start\n${err}`);
				reject(err);
			});
		});
	}
	stop(){
		return new Promise(resolve => this._client.destroy().finally(() => resolve()))
	}
	use(handler){
		if(handler instanceof require('./CommandHandler'))
			this._commandHandlers.push(handler);
		if(handler instanceof require('./ReactionHandler'))
			this._reactionHandlers.push(handler);
	}
	channels(){
		return this._client.channels;
	}
	guilds(){
		return this._client.guilds;
	}
	users(){
		return this._client.users;
	}
	static get(name){
		return Bot.instances.find(bot => bot._name === name);
	}
	static startAll(){
		return Promise.all(Bot.instances.map(bot => bot.start));
	}
	static stopAll(){
		return Promise.all(Bot.instances.map(bot => bot.stop));
	}
}

Bot.instances = [];

module.exports = Bot;
