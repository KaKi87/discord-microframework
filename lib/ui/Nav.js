const RichEmbed = require('discord.js').RichEmbed;

const ReactionHandler = require('../ReactionHandler');

module.exports = class Nav {
	constructor(fastPrev, prev, next, fastNext, user, bots, logLevel, file){
		this.fastPrev = fastPrev;
		this.prev = prev;
		this.next = next;
		this.fastNext = fastNext;
		this.user = user;
		this.bots = bots;
		this.logLevel = logLevel;
		this.file = file;
	}
	messageSent(messageSent){
		return new Promise((resolve, reject) => {
			const
				edit = message => {
					if(!message) return Promise.resolve();
					return messageSent.edit(typeof message === 'string' ? message : new RichEmbed(message));
				},
				getMessageSentEmbed = () => messageSent.embeds.find(e => e.type === 'rich'),
				getMessageSentContent = () => getMessageSentEmbed() ? new RichEmbed(getMessageSentEmbed()) : messageSent.content,
				reactionHandler = new ReactionHandler(messageSent.id, [
					this.fastPrev ? '⏪' : false,
					this.prev ? '◀' : false,
					this.next ? '▶' : false,
					this.fastNext ? '⏩' : false
				].filter(Boolean));
			reactionHandler.listen(messageSent, [
				{
					emoji: '⏪',
					action: () => new Promise((resolve, reject) => this.fastPrev(getMessageSentContent())
						.then(message => edit(message).then(resolve)).catch(reject)),
					remove: true
				},
				{
					emoji: '◀',
					action: () => new Promise((resolve, reject) => this.prev(getMessageSentContent())
						.then(message => edit(message).then(resolve)).catch(reject)),
					remove: true
				},
				{
					emoji: '▶',
					action: () => new Promise((resolve, reject) => this.next(getMessageSentContent())
						.then(message => edit(message).then(resolve)).catch(reject)),
					remove: true
				},
				{
					emoji: '⏩',
					action: () => new Promise((resolve, reject) => this.fastNext(getMessageSentContent())
						.then(message => edit(message).then(resolve)).catch(reject)),
					remove: true
				}
			], this.user, true, this.bots, this.logLevel, this.file).then(() => resolve(reactionHandler)).catch(reject);
		});
	}
};