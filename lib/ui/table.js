const table = require('table').table;

const codeBlock = require('../Utils').codeBlock;

module.exports = data => codeBlock(table(data));