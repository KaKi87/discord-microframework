const
	Observable = require('zen-observable'),
	RichEmbed = require('discord.js').RichEmbed;

const
	Logger = require('./Logger'),
	react = require('./ReactionHandler').react;

module.exports = class CommandHandler {
	constructor(name, prefix, separator, bots, channels, logLevel){
		this._prefix = prefix;
		this._separator = separator || ' ';
		this._bots = bots;
		this._channels = channels;
		this._log = new Logger(name, logLevel || 'error').log;
		this._observable = new Observable(observer => this._observer = observer);
	}
	_handle(message, me){
		if(
			message.author.id === me
			||
			!message.content.startsWith(this._prefix)
			||
			(message.author.bot && !this._bots)
			||
			(this._channels && this._channels.indexOf(message.channel.id) === -1)
		) return;
		this._log(3, `Command handled : "${message.content}" by ${message.author.tag} (${message.author.id})`);
		const
			isNumeric = string => /^-?(\d+\.?\d*|\d*\.?\d+)$/.test(string) && !isNaN(Number(string)) && isFinite(Number(string)),
			args = message.content.slice(this._prefix.length).toLowerCase().split(this._separator).map(arg => isNumeric(arg) ? Number(arg) : arg),
			args_assoc_match = message.content.match(/\w+=([^"\s]+|".+")/g);
		this._observer.next({
			cmd: args[0],
			args: [args.slice(1).join(this._separator), ...args.slice(1)],
			args_assoc: args_assoc_match ? Object.assign({}, ...args_assoc_match
				.map(kv => kv.replace(/"/g, '').split('='))
				.map(kv => ({ [kv[0]]: kv[1] }))) : {},
			del: () => message.delete(),
			out: m => message.channel.send(typeof m === 'string' ? m : new RichEmbed(m)),
			rep: m => message.reply(typeof m === 'string' ? m : new RichEmbed(m)),
			dm: m => message.author.send(typeof m === 'string' ? m : new RichEmbed(m)),
			react: r => react(message, r),
			author: message.author
		});
	}
	subscribe(...args){
	    return this._observable.subscribe(...args);
    }
};
