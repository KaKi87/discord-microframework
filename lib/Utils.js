module.exports = {
	twoDigits: number => number.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }),
	capitalize: string => string[0].toUpperCase() + string.slice(1).toLowerCase(),
	code: string => `\`${string}\``,
	codeBlock: (content, lang) => `\`\`\`${lang ? `${lang}\n` : ''}${content}\`\`\``,
	mentionUser: (user, raw) => `<@${raw ? user.id : user}> ${raw ? `\`[${user.username}#${user.discriminator}]\`` : ''}`,
	mentionChannel: chanId => `<#${chanId}>`,
	randomHexColor: () => '#' + Math.floor(Math.random() * 16777215).toString(16),
	intersect: (array1, array2) => array1.filter(value => -1 !== array2.indexOf(value)),
	emojis: {
		num: [...Array(58).keys()].slice(48).map(n => String.fromCharCode(n, 65039, 8419)),
		alpha: [...Array(56832).keys()].slice(56806).map(n => String.fromCharCode(55356, n))
	},
	embedMargin: { name: '** **', value: '** **' }
};
