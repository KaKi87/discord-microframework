# discord-microframework

Interaction-oriented NodeJS bot micro-framework for Discord.

## Getting started

### Prerequisites

- NodeJS
- NPM
- Yarn

### Install

From [npm](https://www.npmjs.com/package/discord-microframework)

`yarn add discord-microframework`

or

`npm i discord-microframework`

## Use

```js
const Discord = require('discord-microframework');
```

### Bot

#### Instanciation

```js
const Bot = new Discord.Bot(name, token, logLevel);
```

- `name` *string*
- `token` *string*
- `logLevel` *string*

#### Start / Stop

```js
Bot.start();
```

```js
Bot.stop();
```

```js
Discord.Bot.startAll();
```

```js
Discord.Bot.stopAll();
```

### Command handler

#### Instanciation

```js
const myCommandHandler = new Discord.CommandHandler(name, prefix, bots, channels, logLevel);
```

- `name` *string*
- `prefix` *string*
- `bots` *boolean* - Decide wether to handle commands from bots or not.
<br>(default : `false`)
- `channels` *array* - Channels ID where commands are handled.
<br>(default : `null` — means everywhere)
- `logLevel` *string*

#### Commands subscription

```js
myCommandHandler.subscribe(command => {
    // do something
});
```

- `cmd` *string* - Command without prefix and parameters
- `args` *array* - Command parameters
- `args_assoc` *object* - Associative command parameters (parsed as`key=value`)
- `del` *function* - Delete command
- `out` *function* - Output response (short for 'output')
    - `message` *string*
- `rep` *function* - Output response with mention (short for 'reply')
    - `message` *string*
- `dm` *function* - Output response in DM
    - `message` *string*
- `react` *function*
    - `reaction(s)` *string* or *array* - Unicode emoji characters

#### Handler activation

````js
Bot.use(myCommandHandler);
````

### Reaction handler

#### Instanciation

```js
const myReactionHandler = new Discord.ReactionHandler(name, emojis);
```

- `name` *string*
- `emojis` *array* - Unicode emoji characters

#### Handler activation

```js
Bot.use(myReactionHandler);
```

#### Handler listening

```js
myReactionHandler.listen(message, callbacks, user, remove, bots, logLevel);
```

#### Getters

```js
Bot.channels()
Bot.guilds()
Bot.users()
```

#### Classes

```js
Discord.RichEmbed
```

## Built with

Node modules :
- [discord.js](https://github.com/discordjs/discord.js/) - Discord bot API
- [console-stamp](https://github.com/starak/node-console-stamp) - Timestamped logging
- [p-queue](https://github.com/sindresorhus/p-queue) - Ordered Promises resolution
- [zen-observable](https://github.com/zenparsing/zen-observable) - Observables implementation

## Changelog

- `1.0.0` (2019-06-??) • Initial release